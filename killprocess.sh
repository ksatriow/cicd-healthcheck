#!/bin/bash

# Check if a port number was provided as an argument
if [ -z "$1" ]; then
  echo "Usage: $0 <PORT>"
  exit 1
fi

PORT="$1"

# Find the PID of the Node.js process running on the specified port
PID=$(lsof -ti :$PORT)

if [ -z "$PID" ]; then
  echo "No Node.js process found on port $PORT"
  exit 1
fi

# Terminate the Node.js process
kill -9 $PID
echo "Node.js process with PID $PID on port $PORT has been terminated."